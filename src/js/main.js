function responsive() {
	let width = $('.js-calc-height').width();
	$('.js-calc-height').height(width)
}

$(document).ready(function () {
	responsive()

	$(window).resize(function() {
		responsive()
	});

	$('.js-label-item').on('mouseup', function () {
		$('.js-label-item').removeClass('is-active');
		$(this).addClass('is-active')
		let target = $(this).data().target;
		if (target == undefined) {
			$('.js-menu-screen').removeClass('is-visible');
			return;
		}
		$('.js-menu-screen').removeClass('is-visible');
		$(target).addClass('is-visible')
	})


	$('.eset-feature__more-link').on('mouseup', function (e) {
		
		$(this).toggleClass('open');
		if ( $(this).hasClass('open') ) {
			$(this).text('Менше');
		} else {
			$(this).text('Більше');
		}
		
		let droplist = $(this).closest('.eset-features__inner-row').find('.f-droplist')
		droplist.slideToggle()
	});

	$('.f-droplist-item__title').on('mouseup', function (e) {
		if ( $(this).hasClass('open') ) {
			$('.f-subdrop').slideUp();
			$(this).removeClass('open')
			return;
		}
		$('.f-droplist-item__title').removeClass('open');
		$('.f-subdrop').slideUp();
		$(this).toggleClass('open');
		$(this).closest('.f-droplist-item').find('.f-subdrop').slideToggle();
	})


	//bitrix calculator
	$('.bitrix-tariff__price-holder').on('click', function(e) {
		if ( $(e.target).is('.js-b24') ) {
			$(this).find('.js-b24').removeClass('active')
			let data = $(e.target).data('value')
			let target = $(this).find('.js-b24-price').text(data);
			$(e.target).addClass('active');
		}
	})
	
})

